import Data.List

-- Random number generation
data State = State Int deriving (Show)

-- Initialize the random state
initRandomState s = State s

-- Calculate a new random object and return the number along with the new state
random :: State -> (State, Int)
random (State x) = (State newX, x)
        where a = 333667
              b = 213453321
              m = 1000000000
              newX = (a*x+b) `mod` m

-- Calulate the "value" of a cards.  Inputting an king should return a 12, ace returns 0
-- a five should return 5, and so on.  The purpose is to convert the list representation
-- to a usable cards value
baseValue x = (x `mod` 13) + 1

-- Compare the value of two cards
baseValueCompare a b 
    | x < y = LT
    | x > y = GT
    | x == y = EQ
    where x = baseValue a
          y = baseValue b

-- Sort a list of cards by their value 
sortByValue l = sortBy baseValueCompare l

-- Initialize a shuffled list of integers 1,2..51.  The list of integers respresents the
-- deck of cards, where the integers 0-12 represent all clubs, 13-25 represent all diamonds, and so on
initCards :: Int -> Cards
initCards s = 
    let (shuffled,newRandomState) = shuffle [0,1..51] 0 (initRandomState s)
    in Cards shuffled [] newRandomState

-- A type to represent the state of the deck
data Cards = Cards { deck :: [Int]
                    , discarded :: [Int]
                    , cardsState :: State} deriving Show

-- A type to respresent a player
data Player = Player { money :: Int
                     , rounds :: Int
                     , playerId :: Int} deriving Show

-- Compare the IDs of two players.  This is used to sort lists of players
playerIdCompare a b
    | x < y = LT
    | x > y = GT
    | x == y = EQ
    where x = playerId a
          y = playerId b

-- Map a list of players onto a list of their money
getPlayersMoney :: [Player] -> [Int]
getPlayersMoney players = map (\x -> money x) players

-- Initialize a list of players with IDs 1,2..n
initPlayers :: Int -> Int -> Int -> [Player]
initPlayers _ 0 _ = []
initPlayers m n total = Player m 0 (total-n): initPlayers m (n-1) total

-- Shuffle a list of "cards" using the random number generator function.
-- This function follows the description given in the assignment
shuffle :: [Int] -> Int -> State -> ([Int],State)
shuffle cards n randomState =
    let b = length cards -- Get the length of the list
        (newState,randNum) = random randomState -- Get a new random number
        value = cards !! (randNum `mod` b) -- Get the values at the two indices to be swapped
        value2 = cards !! n
    in
        -- If we haven't reached the end of the list, recursively call shuffle 
        -- on the list resulting from swapping the two values
        if n < b then shuffle (map (\x -> if x == value then value2
                                         else if x == value2 then value
                                         else x) cards) (n+1) newState
        else (cards,randomState)

-- This function takes a Cards type, removes the item at the head of the deck
-- and returns it along with a modified cards object to reflect the change.
-- If the deck is empty, the discard pile is shuffled and added back to the deck, and a
-- card is subsequently drawn.
drawCard :: Cards -> (Int,Cards)
drawCard c
    -- If the deck is empty
    | null (deck c) = 
        -- Shuffle the discard pile
        let (shuffled,newState) = shuffle discard 0 randomState
        -- Draw a card
        in drawCard (Cards shuffled [] newState)
    | otherwise = 
        -- Draw a card and return the resulting deck
        let (x:xs) = deck c
        in (x,Cards xs discard randomState)
    where discard = discarded c
          randomState = cardsState c

-- Put all the "cards" into the discard pile and return a modified
-- Cards type
discardCards :: [Int] -> Cards -> Cards
discardCards hand cards =
    Cards currentDeck (hand ++ currentDiscarded) randomState
    where currentDeck = deck cards
          currentDiscarded = discarded cards
          randomState = cardsState cards

-- Helper function to fold a hand to compute its value
handleValueFold :: Int -> Int -> Int
handleValueFold a y
    | x == 1 && y < 11 = 11+y
    | x >= 10 = 10+y
    | otherwise = x+y
    where x = baseValue a

-- Compute the value of a hand by using fold.  An important part of this step
-- is sorting the hand before calculating the value, so that the multiple values of aces
-- can be accounted for.
handValue :: [Int] -> Int
handValue [] = 0
handValue h =
    foldr handleValueFold 0 (sortByValue h)

-- Functions for dealer and player choices

-- An enum for representing different choices during the game
data Choices = Stand | Hit | DoubleDown deriving (Enum, Show, Eq)

-- Calculate the choice for a dealer based on his current hand
dealer :: [Int] -> Int -> Int -> Choices
dealer h _ _
    | x >= 17 = Stand
    | otherwise = Hit
    where x = handValue h

-- Calculate the choice for a player based on their current hand, the bet amount,
-- and the amount of money they have
player :: [Int] -> Int -> Int -> Choices
player h b money
    | x > 8 && x < 12 && money >= b*2 = DoubleDown
    | x < 17 = Hit
    | otherwise = Stand
    where x = handValue h

-- Game functions

-- Recursively perform a full turn for a player
-- If the player chooses to hit, the function is recursively called with the new cards and hand
turn bet money choiceFunc cards hand
    | choice == Stand = (bet, hand, cards)
    | choice == Hit = turn bet money choiceFunc newCards newHand
    | choice == DoubleDown = (bet*2, newHand, newCards)
    where
        choice = choiceFunc hand bet money 
        (newCard,newCards) = drawCard cards
        newHand = hand ++ [newCard]

-- Do the turns for a given number of players.  This function is recursively called
-- and creates a list of "result" tuples to be returned
doPlayerTurns :: [Int] -> [[Int]] -> Int -> Cards -> [(Int, [Int], Cards)]
doPlayerTurns [] [] _ _ = []
doPlayerTurns (x:xs) (y:ys) b c =
    let current_turn = if x < b then (0,[],c) else turn b x player c y
        (_,_,cards) = current_turn
    in current_turn : doPlayerTurns xs ys b cards

-- Compute the new value of the players hand given the dealers hand and theirs
-- All of the cases below are given in the assignment description
getTurnResult :: Int -> Int -> (Int, [Int], Cards) -> Int
getTurnResult dealer_value old_money turn_result
    | dealer_value /= 21 && player_value == 21 = old_money + (2 * bet)
    | player_value > 21 = old_money - bet
    | dealer_value > 21 = old_money + bet
    | player_value > dealer_value = old_money + bet
    | player_value == dealer_value = old_money
    | otherwise = old_money - bet
    where
        (bet,hand,_) = turn_result
        player_value = handValue hand

-- Return a list of dealt cards and their resulting Cards states
-- This is used to deal a card to each player in a row, without giving each one
-- both of their cards at once.
dealCards :: [Int] -> Cards -> [(Int,Cards)]
dealCards [] _ = []
dealCards (_:xs) cards = 
    let (card1,cards1) = drawCard cards
    in (card1,cards1) : dealCards xs cards1

-- Combine the two cards for each player into a single "hand" list
-- This function is used with zipWith to merge lists
combineResults :: (Int,Cards) -> (Int,Cards) -> [Int]
combineResults x y = [fst x, fst y]

-- Return the dealers starting hand, and the starting hands for all players
dealStartingCards players cards = 
        -- Deal each player a card
    let player_hands1 = dealCards players cards
        cards1 = snd $ last player_hands1
        -- Deal the dealer a card...
        dealer_hand1 = dealCards [0] cards1 
        cards2 = snd $ last dealer_hand1
        -- Deal each player another card
        player_hands2 = dealCards players cards2
        cards3 = snd $ last player_hands2
        -- Deal the dealer another card...
        dealer_hand2 = dealCards [0] cards3
        cards4 = snd $ last dealer_hand2
    -- Return the resulting hands
    in (zipWith combineResults dealer_hand1 dealer_hand2, zipWith combineResults player_hands1 player_hands2, cards4)

-- Take in lists of turn result objects (returned by doPlayerTurns) and discard all of the cards inside
-- them in the correct order
discardAllCards :: [(Int, [Int], Cards)] -> Cards -> Cards
discardAllCards [] c = c
discardAllCards (x:xs) cards = 
    discardAllCards xs (discardCards hand cards)
    where (_,hand,_) = x

-- Perform an entire round of play, and return the resulting money values and card state
bj_round :: Int -> [Int] -> Int -> Cards -> ([Int], Int, Cards)
bj_round b players dealer_money cards = 
        -- Get the starting hands for the players and the dealer
    let (dealer_hand,player_hands,dealt_cards) = dealStartingCards players cards
        -- Perform all player Turns
        player_results = doPlayerTurns players player_hands b dealt_cards
        (_,_,player_cards_result) = last player_results
        -- Do the dealers turn
        dealer_result = turn b dealer_money dealer player_cards_result (head dealer_hand)
        (_,dealer_result_hand,result_cards) = dealer_result
        -- Calculate the value of the dealers hand
        dealer_hand_value = handValue dealer_result_hand
        players_new_money = zipWith (\x y -> getTurnResult dealer_hand_value x y) players player_results
        -- Compute a new list of players money
        new_dealer_money = dealer_money + sum (zipWith (\x y -> x - y) players players_new_money)
        -- Discard all cards and get the output Cards state
        final_cards = discardAllCards player_results result_cards
        final_cards2 = discardAllCards [dealer_result] final_cards
    in
        (players_new_money, new_dealer_money, final_cards2)

-- Check if no more players can bet in a new round
game_finished :: Int -> [Player] -> Bool
game_finished b players = all (\x -> (money x) < b) players

-- Recursively run all of the rounds in the game.  If a player no longer has enough money to
-- play, they are put in a special "eliminated" list that no longer plays.
-- This function returns a tuple containing a list of Player types still in the game, a list
-- of Player types eliminated, the amount of money the dealer has, and the final Cards state
doRounds :: Int -> Int -> [Player] -> [Player] -> Int -> Cards -> ([Player], [Player], Int, Cards)
doRounds b r players eliminated dealer_money cards =
        -- Run the round
    let round_result = bj_round b (getPlayersMoney players) dealer_money cards
        (players_money,dealer_new_money,new_cards) = round_result
        -- Modify the players list to include the new money values
        new_players = zipWith (\x y -> Player y ((rounds x) + (if (money x) < b then 0 else 1)) (playerId x)) players players_money
        -- Put the players in either the "still playing" list or the "eliminated" list
        still_playing = filter (\x -> (money x) >= b) new_players
        now_eliminated = (filter (\x -> (money x) < b) new_players) ++ eliminated
    in  if (game_finished b new_players) || r == 1 then (still_playing,now_eliminated,dealer_new_money,new_cards) 
        else doRounds b (r-1) still_playing now_eliminated dealer_new_money new_cards

-- Create a string to represent the output of the game for all players
printPlayers :: [Player] -> [Char]
printPlayers [x] = (show (rounds x)) ++ "#" ++ (show (money x))
printPlayers (x:xs) = (show (rounds x)) ++ "#" ++ (show (money x)) ++ " " ++ (printPlayers xs)

-- Main entry point to the program.  The variables s, n, m, b, and r are the same as described
-- in the assignment description
blackjack s n m b r = 
        -- Get the list of players
    let players = initPlayers m n n
        -- Get the initial Cards state
        cards = initCards s
        -- Perform all rounds of the game
        output = doRounds b r players [] 0 cards
        (final_players,eliminated_players,dealer_value,_) = output
        -- Combine all players and sort them by their id
        all_players = final_players ++ eliminated_players
        sorted_players = sortBy playerIdCompare all_players
        printed_players = "[" ++ (printPlayers sorted_players) ++ "]"
        -- Print the output
    in putStrLn $ "game(" ++ (show dealer_value) ++ " " ++ printed_players ++ ")"
