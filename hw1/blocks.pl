%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Command idiom c:
%  {Please} place <put> {block} X on <onto >{block} Y, W on Z, ...
%  I want <would like> X on Y, W on Z, ...
%  I want <would like> you to put <place> ...
%  Can <could> <would> you {please} put <place> X on Y, ...

c(L) --> lead_in,arrange(L),end.

end --> ['.'] | ['?'].

lead_in --> please, place.
lead_in --> [i], [want] | [i], [would], [like], you_to_put.
lead_in --> ([can] | [could] | [would]), [you], please, place.

you_to_put --> [] | [you], [to], place.   %%% partially optional

please --> [] | [please].    %%% optional word

place --> [put] | [place].   %%% alternate words

arrange([ON]) --> on(ON).
arrange([ON|R]) --> on(ON), comma, arrange(R).

comma --> [','] | ['and'] | [','],[and].   %%% alternate words

on(on(X,Y)) --> block, [X], ([on] | [onto] | [on],[top],[of]), block, [Y]. % Put/place block X onto block Y
on(on(X,table)) --> block, [X],([on] | [onto]), [the], [table]. % Put X onto the table
on(put_all_in_pile) --> [all], [of], [the], [blocks], [in], [a], [single], [pile]. % Put all of the block in a single pile
on(move_from_top_to(A,B)) --> [the], block, [on], [top], [of], block, [A], [on], [top], [of], block, [B]. % Put the block on top of X on top of Y
on(move_from_top_to(A,table)) --> [the], block, [on], [top], [of], block, [A], [on], [the], [table]. % Put the block on top of X on the table
on(move_highest_to(table)) --> [the], [highest], [block], [on], [the], [table]. % Put the highest block on the table
on(move_highest_to(A)) --> [the], [highest], [block], [on], [top], [of], block, [A]. % Put the highest block on top of block A

block --> [] | [block].   %%% optional word

:- [read_line].

% Load the specified file, then start the main input loop
load_then_place(File) :-
    load_script(File),
    place_blocks.

% Read in commands and execute them, based on the read_line function
place_blocks :-
    repeat,
    write('?? '),
    read_line(X),
    ( c(F,X,[]), assert_list(F), write('ok.'), nl | q(F,X,[]) ),
    answer(F), nl, fail.

% Assert each item in the list.
assert_list([]).
assert_list([H|T]) :- assert_item(H), assert_list(T).

% Add a new table spot.
assert_table_spot([X,0]) :- assert(free_spot_on_table([X,0])).
assert_table_spot([_,Y]) :- Y \= 0.

% Move block A on the table.
assert_item(on(Ai,table)) :-
    get_value(Ai, A),
    location(A,[X,Y]),
    not(Y is 0),
    free_spot_on_table(P),
    YN is Y + 1,
    not(location(_,[X,YN])),
    retract(free_spot_on_table(P)),
    retract(location(A,[X,Y])),
    assert(location(A,P)),!,
    nb_setval(result, A). % Assign the result to the global variable
% Move block A on block B.
assert_item(on(Ai,Bi)) :-
    get_value(Ai, A),
    get_value(Bi, B),
    B \== table,
    location(A, [XA,YA]),
    YAN is YA + 1,
    not(location(_, [XA,YAN])),
    location(B, [XB,YB]),
    YBN is YB + 1,
    not(location(_, [XB,YBN])),
    retract(location(A, [XA,YA])),
    assert_table_spot([XA,YA]), % Possibly free up spot on table.
    assert(location(A, [XB,YBN])),!,
    nb_setval(result, A). % Assign the result to the global variable
% Handle errors.
assert_item(on(A,table)) :-
    location(A,[_,Y]), Y is 0,
    write('Already on the table!'), nl, !, fail.
assert_item(on(_,table)) :-
    not(free_spot_on_table(_)),
    write('No free spots on the table!'), nl, !, fail.
assert_item(on(A,table)) :-
    location(A,[X,Y]), YN is Y + 1, location(_,[X,YN]),
    write('Cannot move from, something is on top!'), nl, !, fail.
assert_item(on(A,_)) :-
    not(location(A, _)),
    write('Block to move does not exist!'), nl, !, fail.
assert_item(on(_,B)) :-
    B \== table, not(location(B, _)),
    write('Block to place on does not exist!'), nl, !, fail.
assert_item(on(A,B)) :-
    B \== table, location(A, [XA,YA]), YAN is YA + 1, location(_, [XA,YAN]),
    write('Cannot move from, something is on top!'), nl, !, fail.
assert_item(on(_,B)) :-
    B \== table, location(B, [XB,YB]), YBN is YB + 1, location(_, [XB,YBN]),
    write('Cannot move to, something is on top!'), nl, !, fail.

% If the input is result, return the value the global is assigned to.
get_value(result, O) :-
    nb_getval(result, O).
% If the input isn't result, just return itself.
get_value(I, I).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Question idiom q:
%   Which block is on top of X?
%   What is on top of?
%
:- op(500, xfx, 'is_on_top_of'). % Define an operator for "is on top of"
:- op(500, xfx, 'is_sitting_on'). % Define and operator for "is X sitting on"

% Create grammars to match different possible questions.
q(_ is_on_top_of A) --> [which],[block],[is],[on],[top],[of],block,[A],end. % Which block is on top of X
q(_ is_on_top_of A) --> [what],[is],[on],[top],[of],block,[A],end. % What is on top of X
q(A is_sitting_on _) --> [what],[is],block,[A],[sitting],[on],end. % What is X sitting on
q(A is_sitting_on _) --> [what],[is],[below],block,[A],end. % What is below X
q(all_on_table(_)) --> [what],[blocks],[are],[on],[the],[table],end. % What blocks are on the table
q(load_script(F)) --> [load],[script],[F],end. % load script X (no quotes allowed)

% Find the block on top of Ai by checking if it has a block above it.
B is_on_top_of Ai :- 
    % Get the value of the input if it is result
    get_value(Ai, A),
    location(A,[X,Y]),
    Y1 is Y+1,
    location(B,[X,Y1]), !,
    nb_setval(result, B).
'Nothing' is_on_top_of _ .

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Question idiom q:
% 	What is block X sitting on?
%	What is below block X?

% Find the block Ai is sitting on by looking at the location under it
Ai is_sitting_on B :- 
    % Get the value of the input if it is result
    get_value(Ai, A),
    location(A, [X,Y]),
    Y1 is Y-1,
    location(B, [X, Y1]), !,
    nb_setval(result, B).
_ is_sitting_on 'the table'.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Question idiom q:
% 	What blocks are on the table?
%	Which blocks are on the table?

% Return a list of all blocks that are on the table.
all_on_table(L) :- findall(X, on(X,table), L).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Question idiom q:
% 	Put all of the blocks in a single pile
%	Put all blocks in a single pile

% Get the list of all blocks, retract their locations, then stack them starting from Y=0
put_all_in_pile :- findall(X, location(X,_), L),
                   retractall(location(_, _)),
                   stack(L, 0),
                   assert_table_spot([1,0]),
                   assert_table_spot([2,0]).

% Set the location of the block at the head of the list to [0,Y], then recursively set the next one on top.
stack([], _).
stack([H|T], Y) :- 
    assert(location(H, [0,Y])),
    Ynew is Y + 1,
    stack(T, Ynew).

assert_item(put_all_in_pile) :- put_all_in_pile.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Command idiom:
%   Put the block on top of X on top of block Y
%   Put the block on top of X on the table

% Check if both blocks are 'result' and replace with the appropriate block, then try to assert the block on top of
% A to the top of B.
move_from_top_to(A, B) :-
    get_value(A, A1),
    get_value(B, B1),
    on(X, A1),
    assert_item(on(X, B1)), !,
    nb_setval(result, X).
assert_item(move_from_top_to(A, B)) :- move_from_top_to(A,B).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Command idiom:
%   Put the highest block on top of block y
%   Put the highest block on the table

% Check for a block that doesn't have another block higher than it.
find_highest(X) :-
    location(X, [_,Y]),
    Y2 is Y + 1,
    not(location(_, [_,Y2])).

% Check if the input block is 'result' and if so replace it, then find the highest block using "find_highest"
% and put it on top of the input block
move_highest_to(Ai) :-
    get_value(Ai, A),
    find_highest(X),!,
    assert_item(on(X, A)),
    nb_setval(result, X).
assert_item(move_highest_to(A)) :- move_highest_to(A).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Load from a file.  This only works if the filename has no file extension. The predicate load_then_place should be used instead.
% load script <file>

load_script(File) :- see(File).
   
answer(X is_on_top_of A) :- call(X is_on_top_of A),
                            say([X,is,on,top,of,A]).
% Print "A is sitting on X"
answer(A is_sitting_on X) :- call(A is_sitting_on X),
							 say([A,is,sitting,on,X]).

% Print "B1 B2 B3 ..." where Bn is a block on the table.
answer(all_on_table(L)) :- call(all_on_table(L)),
							 say(L).

% Print "all blocks are now in a pile".
answer(put_all_in_pile) :- call(put_all_in_pile),
						   say([all,blocks,are,now,in,a,pile]).

% Print "loaded file F" where F is the name of the loaded file (again with no extension)
answer(load_script(F)) :- call(load_script(F)),
                          say([loaded,file,F]).

say([X|R]) :- write(X), write(' '), say(R).
say([]).

%
%  positioning information
%
%  [0,3] [1,3] [2,3]
%  [0,2] [1,2] [2,2]
%  [0,1] [1,1] [2,1]
%  [0,0] [1,0] [2,0]
% -=================-   table
%
% initially
%
%    c
%    b
%    a     d
% -=================-

:- dynamic free_spot_on_table/1.
:- dynamic location/2.
:- dynamic on/2.

free_spot_on_table([2,0]).

location(c,[0,2]).
location(b,[0,1]).
location(a,[0,0]).
location(d,[1,0]).

on(A,table) :- location(A,[_,0]).
on(A,B) :- B \== table,
           location(A,[X,YA]),
           location(B,[X,YB]),
           YB is YA - 1.
